# sit 1.1.2

* Automatically clean up dummy surveys (counts of 0 individuals surveyed
__before__ their release date, which are typically residual artefacts of
wide-shaped data sets that were transformed for importing into `sit`).


# sit 1.1.0

* Support traps identified with site-id and trap-type, as in the Albania case
study.


# sit 1.0.0

* First fully functional version of the package. Used in the first training
at IAEA from 2021-12-06 to 2021-12-10.

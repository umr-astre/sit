## Reproduce the CI environment with:
docker run --rm -it \
  -v "$(pwd)":"/opt/$(basename $(pwd))" \
  -w "/opt/$(basename $(pwd))" \
  -e R_LIBS_USER="/opt/$(basename $(pwd))/ci/lib" \
  -e CHECK_DIR="/opt/$(basename $(pwd))/ci/logs" \
  -e BUILD_LOGS_DIR="/opt/$(basename $(pwd))/ci/logs/$(basename $(pwd)).Rcheck" \
  rocker/geospatial bash

test_that("Importing egg surveys works without errors.", {

  res <- expect_error(sit_egg_surveys(fake_eggs), NA)

  expect_s3_class(res, "sit_egg_surveys")

})

test_that("Detect duplicate surveys.", {

  tdf <- fake_eggs[c(1:3, 2),]

  expect_error(sit_egg_surveys(tdf), 'Row 4 duplicated')

})

test_that("Exaclty one of activation or duration are required.", {

  tdf <- fake_eggs
  tdf$duration <- NULL

  expect_error(sit_egg_surveys(tdf), 'Both are currently missing')

  tdf <- fake_eggs
  tdf$activation <- fake_eggs$survey

  expect_error(sit_egg_surveys(tdf), 'Please remove one')

})

test_that("Use activation dates.", {

  tdf <- fake_eggs
  durations <- as.difftime(c(7, 5, 6, 5), units = "days")

  tdf$duration <- NULL
  tdf$activation <- as.character(as.POSIXct(tdf$survey) - durations)

  res <- expect_error(sit_egg_surveys(tdf), NA)

  expect_identical(durations, res$datetime_end - res$datetime_start)
})

test_that("Check that starting times precedes ending times.", {

  tdf <- fake_eggs
  tdf$duration[2] <- -5

  expect_error(sit_egg_surveys(tdf), "duration")


  tdf$activation <- as.character(
    as.POSIXct(tdf$survey) - as.difftime(tdf$duration, units = "days"))
  tdf$duration <- NULL

  expect_error(sit_egg_surveys(tdf), "activation")


})

## Other checks TODO:
## - activation precedes survey, or that duration is positive
## - None of the mandatory arguments are missing and are of the correct types


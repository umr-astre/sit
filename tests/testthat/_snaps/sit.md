# Print sit

    Code
      print(sit_prototype)
    Output
      -- sit -------------------------------------------------------------------------
      Release events: 1 (areal) + 3 (point)
      Release sites : 2
      Adult traps   : 21 (sit)
      Egg traps     : 7 (control) + 14 (sit)
      Trap types    : 21 (BGS) + 21 (OVT)
      Surveys       : 3486 (adult) + 462 (egg)

# Summarise sit

    Code
      print(summary(sit_prototype))
    Output
      -- Releases --------------------------------------------------------------------
      Number of point releases: 1 
      Number of areal releases: 1 
      
      Release events:
        id  type site_id       date colour     n    geometry
      1  1 point       1 2019-11-25 yellow 10000 POINT (0 0)
      2  2 point       1 2019-12-01    red 10000 POINT (0 0)
      3  3 point       1 2019-12-13   blue 10000 POINT (0 0)
      4  4 areal       2 2019-12-21   pink 10000 POINT EMPTY
      
      Release sites:
      Simple feature collection with 2 features and 1 field (with 1 geometry empty)
      Geometry type: POINT
      Dimension:     XY
      Bounding box:  xmin: 0 ymin: 0 xmax: 0 ymax: 0
      CRS:           +proj=laea +x_0=0 +y_0=0 +lon_0=16.41472 +lat_0=48.23528
        id    geometry
      1  1 POINT (0 0)
      2  2 POINT EMPTY
      
      -- Traps -----------------------------------------------------------------------
      Number of traps in each group:
           Area Type Coords  n
      1 control  OVT  FALSE  7
      2     sit  OVT  FALSE 14
      3     sit  BGS   TRUE 21
      
      -- Adult surveys (total captures) ----------------------------------------------
      , , sit
      
             female male
      yellow     NA   58
      red        NA   79
      blue       NA   86
      pink       NA   90
      wild      226  299
      
      
      -- Egg surveys (total captures) ------------------------------------------------
              Sterile Fertile
      control    3407   18432
      sit        4488    6808
      
      -- sit -------------------------------------------------------------------------
      Release events: 1 (areal) + 3 (point)
      Release sites : 2
      Adult traps   : 21 (sit)
      Egg traps     : 7 (control) + 14 (sit)
      Trap types    : 21 (BGS) + 21 (OVT)
      Surveys       : 3486 (adult) + 462 (egg)

